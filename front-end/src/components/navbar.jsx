import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

//import '../index.css';
function Navigationbar() {
    const navbarStyle = {
        backgroundColor: '#4e59e0', 
        color: 'white'
      };
    
    return (
      <>
        <Navbar style={navbarStyle} variant="dark">
          <Container>
            <Navbar.Brand href="home">Estacionamiento</Navbar.Brand>
            <Nav className="me-auto">
              {/* <Nav.Link href="home">Home</Nav.Link> */}
              <Nav.Link href="autos">Autos</Nav.Link>
              <Nav.Link href="addautos">Agregar Autos</Nav.Link>
              <Nav.Link href="graficos">Graficos</Nav.Link>
              <Nav.Link href="estacionamiento">Estacionamiento</Nav.Link>
              <Nav.Link href="mediciones">Mediciones</Nav.Link>
            </Nav>
          </Container>
        </Navbar>

      </>
    );
  }
  
  export default Navigationbar;