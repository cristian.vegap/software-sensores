import {BrowserRouter, Routes, Route } from "react-router-dom";
import  Autos from "./pages/Autos";
import  Mediciones from "./pages/Medicion";
import  AddAutos from "./pages/AddAutos";
import  UpdateAutos from "./pages/UpdateAutos";
import  Home from "./pages/Home";
import  Graficos from "./pages/Graficos";
import Estacionamiento from "./pages/Estacionamiento";
import "./style.css"
//import "./App.css"
//import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <div className="content-container">
        <BrowserRouter>
          <Routes>
            <Route path= "/" element={<Autos/>}/>
            <Route path= "/home" element={<Home/>}/>
            <Route path= "/graficos" element={<Graficos/>}/>
            <Route path= "/autos" element={<Autos/>}/>
            <Route path= "/mediciones" element={<Mediciones/>}/>
            <Route path= "/addautos" element={<AddAutos/>}/>
            <Route path= "/update/:id" element={<UpdateAutos/>}/>
            <Route path= "/estacionamiento" element={<Estacionamiento/>}/>
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;