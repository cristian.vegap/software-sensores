import axios from "axios";
import React, { useState, useEffect } from "react";
import { Link, useLocation, useNavigate} from "react-router-dom";

const UpdateAutos = () => {
    const [auto, setAuto] = useState({
        Dueno:"",
        Telefono:null,
        Patente:null,
    });

    const [error,setError] = useState(false)
    const location = useLocation();
    const navigate = useNavigate();

    const autoId = location.pathname.split("/")[2];


    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/autos/${autoId}`);
                const { Dueno, Telefono, Patente } = response.data; // Suponiendo que la respuesta tiene campos Dueno, Telefono y Patente
                setAuto({ Dueno, Telefono, Patente });
            } catch (error) {
                console.error(error);
            }
        };
    
        fetchData();
    }, [autoId]);
    

    const handleChange = (e) => {
        setAuto((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
    
        try {
          await axios.put(`http://localhost:8080/autos/${autoId}`, auto);
          navigate("/");
        } catch (err) {
          console.log(err);
          setError(true);
        }
      };

    return (
        <div className='form'>
            <br />
            <h1>Editar auto</h1>
            <br />
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="Dueno">Dueño</label>
                    <input
                        type="text"
                        className="form-control"
                        id="Dueno"
                        placeholder={auto.Dueno}
                        onChange={handleChange}
                        name="Dueno"
                        value={auto.Dueno}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="Telefono">Telefono</label>
                    <input
                        type="text"
                        className="form-control"
                        id="Telefono"
                        placeholder={auto.Telefono}
                        onChange={handleChange}
                        name="Telefono"
                        value={auto.Telefono}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="Patente">Patente</label>
                    <input
                        type="text"
                        className="form-control"
                        id="Patente"
                        placeholder={auto.Patente}
                        onChange={handleChange}
                        name="Patente"
                        value={auto.Patente}
                    />
                </div>
                <br/>
                <button type="submit" className="btn btn-primary">
                    Editar
                </button>
                <button type="button" className="btn m-1" style={{ backgroundColor: 'gray', color: "white" }}onClick={() => navigate('/autos') }>Volver</button>
            </form>
        </div>
    )
}

export default UpdateAutos