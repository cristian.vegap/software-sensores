import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import axios from "axios"

const Add = () => {
    const [auto, setAuto] = useState({
        Dueno:"",
        Telefono:null,
        Patente:null,
    });

    //Alertas
    const [successMessage, setSuccessMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);

    const navigate = useNavigate()

    const handleChange = (e) => {
        setAuto(prev=>({...prev, [e.target.name]: e.target.value}));
    };

    const handleClick = async e=>{
        e.preventDefault()
        try{
            await axios.post("http://localhost:8080/autos", auto)
            //setSuccessMessage("¡Auto creado exitosamente!");
            //setErrorMessage(null);
            navigate("/", { state: { successMessage: "¡Auto creado exitosamente!" } });
        }catch(err){
            //setErrorMessage("Error al crear el auto. Por favor, inténtalo de nuevo.");
            //setSuccessMessage(null);
            console.log(err)
        }
    }
    console.log(auto)

    const boton = {
        backgroundColor: '#4e59e0', 
        color: 'white' 
    };

    return (
        <form>
            <h1>Agregar auto nuevo</h1>
            <br />
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Dueño</label>
                <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nombre completo" onChange={handleChange} name="Dueno"/>
            </div>
            <br />
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Telefono</label>
                <input type="number" className="form-control" id="exampleInputPassword1" placeholder="(+569) xxxx xxxx" onChange={handleChange} name="Telefono"/>
            </div>
            <br />
            <div className="form-group">
                <label htmlFor="exampleInputPassword2">Patente</label>
                <input type="text" className="form-control" id="exampleInputPassword2" placeholder="Patente" onChange={handleChange} name="Patente"/>
            </div>
            <br />
            <button type="button" className="btn btn-primary m-1" onClick={handleClick} style={boton}>Agregar</button>
            <button type="button" className="btn m-1" style={{ backgroundColor: 'gray', color: "white" }}onClick={() => navigate('/autos') }>Volver</button>
        </form>

    )
}

export default Add