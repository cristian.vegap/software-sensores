import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Graficos = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    axios
      .get('https://thingspeak.com/channels/2322712/charts/1,2.json?api_key=CE4FSDZFMI91RYDH')
      .then(response => {
        setData(response.data.feeds);
      })
      .catch(error => {
        console.log(error);
      });
  }, []); // El segundo argumento de useEffect, un array vacío, asegura que el efecto se ejecute solo una vez (equivalente a componentDidMount)

return (
    <div>
        <h1>Gráficos</h1>
        <br />
        <div class="container">
            <div class="row">
                <div class="col">
                    <iframe
                        width="450"
                        height="260"
                        style={{ border: '2px solid #cccccc' }}
                        src="https://thingspeak.com/channels/2322712/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&title=Temperatura+Python&type=line&api_key=CE4FSDZFMI91RYDH"
                    ></iframe>
                </div>
                <div class="col">
                    <iframe 
                        width="450"
                        height="260"
                        style={{border: "2px solid #cccccc"}}
                        src="https://thingspeak.com/channels/2322712/charts/2?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&title=Humedad+Python&type=line&api_key=CE4FSDZFMI91RYDH"
                    ></iframe>
                </div>
            </div>
        </div>
    </div>
  );
};

export default Graficos;

