//import thingspeak from 'thingspeak';

export const obtenerDatosThingSpeak = async (setTiempoLocal, setEstacionamiendo1, setEstacionamiendo2) => {
    const channelID = '2322712'; // Reemplaza con el ID de tu canal ThingSpeak
    const readAPIKey = 'CE4FSDZFMI91RYDH'; // Reemplaza con tu clave de lectura del canal ThingSpeak
    const apiUrl = `https://api.thingspeak.com/channels/${channelID}/feed/last.json?api_key=${readAPIKey}`;
    const UrlField5 = `https://api.thingspeak.com/channels/${channelID}/fields/5/last.json?api_key=${readAPIKey}`;
    //const UrlField1 = `https://api.thingspeak.com/channels/${channelID}/fields/1/last.json?api_key=${readAPIKey}`;
    
    try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        
        const response5 = await fetch(UrlField5);
        const data5 = await response5.json();

        //TIEMPO
        const timefecha5= new Date(data5.created_at).toLocaleString();
        setTiempoLocal(timefecha5)

        // DISPONIBILIDAD ESTACIONAMIENTO 1
        const valorField5 = data5.field5;
        if (valorField5 === '1') {
            setEstacionamiendo1('Ocupado');
        } else if (valorField5 === '0') {
            setEstacionamiendo1('Disponible');
        } else {
            setEstacionamiendo1('Valor no reconocido: '+ valorField5);
            console.log(valorField5)
        }

        // DISPONIBILIDAD ESTACIONAMIENTO 2
        const valorField4 = data.field4;
        if (valorField4 === '1') {
            setEstacionamiendo2('Ocupado');
        } else if (valorField4 === '0') {
            setEstacionamiendo2('Disponible');
        } else {
            setEstacionamiendo2('Valor no reconocido: ' + valorField4);
        }

    } catch (error) {
        console.error('Error al obtener datos de ThingSpeak:', error);
    }
};