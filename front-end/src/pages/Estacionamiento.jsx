import React, {useRef, useState, useEffect } from 'react';
import axios from "axios"
import "../columna.css"
import Dropdown from 'react-bootstrap/Dropdown';
import { obtenerDatosThingSpeak } from "./Thingspeak"
import {ThingSpeakUpdater} from './AgregarHistorial'; // Ajusta la ruta según la ubicación real del componente

const channelID = '2322712';
const readAPIKey = 'CE4FSDZFMI91RYDH';

const Estacionamiento = () => {
    const [autoSeleccionado, setAutoSeleccionado] = useState(undefined);
    const [autos, setautos] = useState([])
    const [autoInfo, setAutoInfo] = useState(null);
    const [Estacionamiendo1, setEstacionamiendo1] = useState('');
    const [Estacionamiendo2, setEstacionamiendo2] = useState('');
    const [tiempoLocal, setTiempoLocal] = useState('');
    const [ultimaMedicion, setMediciones] = useState([]);

    const [ID, setID] = useState(null);

    const [MedicionFuncion, setMedicionFuncion] = useState('');
    const [TiempoFuncion, setTiempoFuncion] = useState('');

    const fetchAllAutos = async ()=>{
        try{
            const res = await axios.get("http://localhost:8080/autos")
            setautos(res.data);
        }catch(err){
            console.log(err)
        }
    }

    useEffect(() => {
        //ThingSpeakUpdater(setMedicionFuncion, setTiempoFuncion);
        // const autoId = 
        fetchAllAutos();
        obtenerDatosThingSpeak(setTiempoLocal, setEstacionamiendo1, setEstacionamiendo2);
        const newdata = async () =>{
            await ThingSpeakUpdater(setMedicionFuncion, setTiempoFuncion);
            // console.log('MedicionFuncion in useEffect:', MedicionFuncion);
            // console.log('TiempoFuncion in useEffect:', TiempoFuncion);
            // await handleChangeDropdown();
            // await agregarMedicion(autoId, MedicionFuncion, TiempoFuncion);
            await handleChangeDropdown();
            // await obtenerAutoIdEspecifico();
            // const id = await obtenerAutoIdEspecifico();
            if (autoSeleccionado!== undefined){
                await agregarMedicion(autoSeleccionado, MedicionFuncion, TiempoFuncion);
            }
            await fetchAllAutos();
            await ThingSpeakUpdater(setMedicionFuncion, setTiempoFuncion);
        }
        newdata();
        const intervalo = setInterval(newdata, 10000); // Configura el intervalo
        // handleChangeDropdown();

        return () => clearInterval(intervalo);
    }, [MedicionFuncion, TiempoFuncion]);

    const UltimaFecha = async (autoId) => {
        try {
            const res = await axios.get(`http://localhost:8080/mediciones/${autoId}`);
            const fechaUltimaMedicion = res.data.length > 0 ? res.data[res.data.length - 1].fecha : null;
            return fechaUltimaMedicion
        } catch (err) {
            console.error('Error al obtener las mediciones por dueño:', err);
        }
    };

    const autoIdRef = useRef();

    const obtenerAutoIdEspecifico = async (autoId) => {
        console.log("autoId: ", autoId)
        return autoId
      };

    const handleChangeDropdown = async (autoId) => {
        // console.log("autoId: ", autoId)
        // obtenerAutoIdEspecifico(autoId)
        autoIdRef.current = autoId;
        if (autoId !== undefined){
            obtenerAutoIdEspecifico(autoId)
            setAutoSeleccionado(autoId);
        }
        const autoSeleccionadoInfo = autos.find((auto) => auto.AutoId === autoId);
        
        setAutoInfo(autoSeleccionadoInfo);

        // await agregarMedicion(autoId, MedicionFuncion, TiempoFuncion);
        fetchAllAutos();
    };

    const agregarMedicion = async (autoId, medicionFuncion, tiempoFuncion) => {
        try {
            // console.log("autoId: ", autoIdRef.current)
            const fecha = new Date(TiempoFuncion);
            const formattedFecha = `${fecha.getFullYear()}-${(fecha.getMonth() + 1).toString().padStart(2, '0')}-${fecha.getDate().toString().padStart(2, '0')} ${(fecha.getHours()).toString().padStart(2, '0')}:${fecha.getMinutes().toString().padStart(2, '0')}:${fecha.getSeconds().toString().padStart(2, '0')}`;
            const fechaTabla = await UltimaFecha(autoId);
            const fechaTablaObj = new Date(fechaTabla);
    
            if (fechaTablaObj.getTime() == fecha.getTime()){
                return console.log("Iguales :P")
            }else{
                await axios.post("http://localhost:8080/mediciones", {
                    Auto: autoId,
                    Medicion: MedicionFuncion,
                    Fecha: formattedFecha,
                });
                return console.log("Agregando medicion: ", MedicionFuncion)
            }
        } catch (error) {
            console.error('Error al crear la medición:', error);
        }
    }

    //BOTON TEST
    const cambiarEstadoParaPruebas = nuevoEstado => {
        setEstacionamiendo1(nuevoEstado);
    };
    const cambiarEstadoParaPruebas2 = nuevoEstado => {
        setEstacionamiendo2(nuevoEstado);
    };

    //setEstado('Ocupado');
    return <div className="row">
        {/* COLUMNA 1 */}
        <div className="column">
            <h1>Estacionamiento 1</h1>
            <br/>
            <div className={`alert ${Estacionamiendo1 === 'Disponible' ? 'alert-success' : 'alert-danger'}`} role="alert">
                {Estacionamiendo1}
            </div>
            <p>Última disponibilidad obtenida: {tiempoLocal}</p>

            <h3>Agregar datos a Automóvil</h3>
            {/* DROP DOWN */}
            <Dropdown>
                <Dropdown.Toggle variant="success" id="autoDropdown">
                    Seleccionar Auto
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {autos.map((auto) => (
                        <Dropdown.Item key={auto.AutoId} onClick={() => handleChangeDropdown(auto.AutoId)}>
                            {auto.Dueno}
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown><br/>
            <div>
                {autoInfo && (
                    <p><b>Dueño seleccionado:</b> {autoInfo.Dueno}</p>
                )}
            </div>
            <br/>
            <iframe
                width="450"
                height="260"
                style={{ border: '2px solid #cccccc' }}
                src="https://thingspeak.com/channels/2322712/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&title=Corriente+1&type=line">
            </iframe>

        </div>
        {/* COLUMNA 2 */}
        <div className="column">
            <h1>Estacionamiento 2</h1>
            <br/>
            <div className={`alert ${Estacionamiendo2 === 'Disponible' ? 'alert-success' : 'alert-danger'}`} role="alert">
                {Estacionamiendo2}
            </div>
            {/* BOTON TEST */}
            <button onClick={() => cambiarEstadoParaPruebas2('Ocupado')}>Cambiar a Ocupado</button>
            <button onClick={() => cambiarEstadoParaPruebas2('Disponible')}>Cambiar a Disponible</button>

            <br/>
            <br/>
            <iframe
                width="450"
                height="260"
                style={{ border: '2px solid #cccccc' }}
                src="https://thingspeak.com/channels/2322712/charts/2?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&title=Corriente+2&type=line">
            </iframe>
        </div>
    </div>; 
};

export default Estacionamiento;
