import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useLocation, Link } from 'react-router-dom';
import { crearHistorial } from "./AgregarHistorial"
import Dropdown from 'react-bootstrap/Dropdown';

const Mediciones = () => {
    const [mediciones, setMediciones] = useState([]);
    const [autoSeleccionado, setAutoSeleccionado] = useState('');
    const [autos, setautos] = useState([])
    const [autoInfo, setAutoInfo] = useState(null);

    const fetchAllAutos = async ()=>{
        try{
            const res = await axios.get("http://localhost:8080/autos")
            setautos(res.data);
        }catch(err){
            console.log(err)
        }
    }

    const fetchMedicionesPorDueno = async (dueno) => {
        try {
            const res = await axios.get(`http://localhost:8080/medicionesPorDueno/${dueno}`);
            setMediciones(res.data);
        } catch (err) {
            console.error('Error al obtener las mediciones por dueño:', err);
        }
    };

    const handleChangeDropdown = async (autoId, dueno) => {
        setAutoSeleccionado(autoId);
        const autoSeleccionadoInfo = autos.find((auto) => auto.AutoId === autoId);
        setAutoInfo(autoSeleccionadoInfo);
        
        try {
            fetchAllAutos();
            await fetchMedicionesPorDueno(autoSeleccionadoInfo["Dueno"])
        } catch (error) {
            console.error('Error al crear la medición:', error);
        }
    };

    useEffect(() => {
 
        fetchAllAutos()
        const intervalo = setInterval(() => {
            fetchAllAutos();
        }, 10000);

        return () => clearInterval(intervalo);
    }, []);
    
    return (
        <div>
            <h1>Medicion Autos</h1>
            <br />
            {/* DROP DOWN */}
            <Dropdown>
                <Dropdown.Toggle variant="success" id="autoDropdown">
                    Seleccionar Auto
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {autos.map((auto) => (
                        <Dropdown.Item key={auto.AutoId} onClick={() => handleChangeDropdown(auto.AutoId)}>
                            {auto.Dueno}
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown><br/>
            <div>
                {autoInfo && (
                    <p><b>Historial seleccionado:</b> {autoInfo.Dueno}</p>
                )}

                {mediciones.length > 0 ? (
                    <div>
                        {/* <p><b>Mediciones:</b></p> */}
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Medición</th>
                                </tr>
                            </thead>
                            <tbody>
                                {mediciones.map((medicion, index) => (
                                    <tr key={medicion.MedicionId}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{medicion.fecha}</td>
                                    <td>{medicion.Medicion}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    ): (
                        <p><b>No hay mediciones disponibles.</b></p>
                )}
            </div>
        </div>
    );
};

export default Mediciones;
