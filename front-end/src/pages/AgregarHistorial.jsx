import axios from 'axios';
import React, { useEffect } from 'react';

export const ThingSpeakUpdater = async (setMedicionFuncion, setTiempoFuncion) => {
  const channelID = '2322712'; // Reemplaza con el ID de tu canal ThingSpeak
  const readAPIKey = 'CE4FSDZFMI91RYDH'; // Reemplaza con tu clave de lectura del canal ThingSpeak
  const UrlField1 = `https://api.thingspeak.com/channels/${channelID}/fields/1/last.json?api_key=${readAPIKey}`;

  try{
    const response = await fetch(UrlField1);
    const data = await response.json();

    const fecha = new Date(data.created_at);
    const tiempoFuncion = `${fecha.getFullYear()}-${(fecha.getMonth() + 1).toString().padStart(2, '0')}-${fecha.getDate().toString().padStart(2, '0')} ${fecha.getHours().toString().padStart(2, '0')}:${fecha.getMinutes().toString().padStart(2, '0')}:${fecha.getSeconds().toString().padStart(2, '0')}`;
    setTiempoFuncion(tiempoFuncion)
    setMedicionFuncion(data.field1)
    // console.log("DENTROOOOOO")
    // console.log("valor Medicion: ", medicionFuncion)
    // console.log("valor Fecha: ", tiempoFuncion)
    //return { medicionFuncion, tiempoFuncion };
  }catch(error){
      console.error('Error al obtener datos de ThingSpeak:', error);
  }

};