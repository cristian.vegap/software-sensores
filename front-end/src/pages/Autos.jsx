import React, { useEffect, useState } from 'react'
import axios from "axios"
import {useLocation, Link } from 'react-router-dom';
//import "./front-end/src/style.css"
const Autos = () => {
    const [autos, setautos] = useState([])
    
    const location = useLocation();
    const [successMessage, setSuccessMessage] = useState(location.state ? location.state.successMessage : null);
    const [errorMessage, setErrorMessage] = useState(location.state ? location.state.errorMessage : null);
    
    useEffect (()=>{
        const timer = setTimeout(() => {
            if (location.state?.successMessage) {
                setSuccessMessage(null);
            }
            setErrorMessage(null);
          }, 1000); 


        const fetchAllAutos = async ()=>{
            try{
                const res = await axios.get("http://localhost:8080/autos")
                //console.log(res)
                setautos(res.data)
            }catch(err){
                console.log(err)
            }
        }
        fetchAllAutos()
        return () => clearTimeout(timer);
    }, [location.state?.successMessage, errorMessage]);

    const handleDelete = async(AutoId)=>{
        try{
            await axios.delete("http://localhost:8080/autos/"+AutoId)
            if (!successMessage) {
                setSuccessMessage("Auto borrado exitosamente.");
            }
            //window.location.reload()
            setautos((prevAutos) => prevAutos.filter((auto) => auto.AutoId !== AutoId));
        }catch(err){
            setErrorMessage("Error al borrar el auto. Por favor, inténtalo de nuevo.");
            console.log(err)
        }
    }

    const boton = {
        backgroundColor: '#4e59e0', 
        color: 'white' 
    };

    return <div>
        <h1>Autos Estacionamiento</h1>
        <br/>

        {successMessage && (
            <div className="alert alert-success" role="alert">
                {successMessage}
            </div>
        )}
        {errorMessage && (
            <div className="alert alert-danger" role="alert">
                {errorMessage}
            </div>
        )}


        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Dueño</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Patente</th>
                </tr>
            </thead>
            <tbody>
            {autos.map((auto, index) => (
                <tr key={auto.AutoId}>
                    <th scope="row">{index + 1}</th>
                    <td>{auto.Dueno}</td>
                    <td>{auto.Telefono}</td>
                    <td>{auto.Patente}</td>
                    <td>
                        <button className='btn btn-danger m-1' onClick={() => handleDelete(auto.AutoId)}>
                            Borrar
                        </button>
                        <button className='btn btn-primary m-1' style={boton}>
                            <Link to={`/update/${auto.AutoId}`} style={boton}>
                                Editar
                            </Link>
                        </button>
                    </td>
                </tr>
            ))}
            </tbody>
        </table>
        <br/>
        <a class="btn btn-primary" href="/addautos" role="button" style={boton}>Agregar Autos</a>
    </div>;
};

export default Autos