import mysql from "mysql2";

const pool = mysql.createPool({
    host:"localhost",
    user:"root",
    password:"cristian.vegap",
    database:"ioevdb",
}).promise()

//GET TODO
export async function getAutos(){
    const [rows] = await pool.query("SELECT * FROM automovil")
    return rows
}

//GET Especifico
export async function getAuto(AutoId){
    const [rows] = await pool.query(`
    SELECT *
    FROM automovil
    WHERE AutoId = ?
    `, [AutoId])
    return rows
}

//CREAR AUTO 
export async function createAuto(Dueno, Telefono, Patente){
    const [result] = await pool.query(`
    INSERT INTO automovil (Dueno, Telefono, Patente)
    VALUES (?, ?, ?)
    `, [Dueno, Telefono, Patente])

    const id = result.insertId;
    return getAuto(id)
}

//DELETE
export async function delAuto(AutoId){
    const [rows] = await pool.query(`
    DELETE FROM automovil
    WHERE AutoId = ?
    `, [AutoId])
    return rows
}

//Editar 
export async function updateAuto(Dueno, Telefono, Patente, AutoId){
    const [result] = await pool.query(`
        UPDATE automovil
        SET Dueno = ?, Telefono = ?, Patente = ?
        WHERE AutoId = ?
    `, [Dueno, Telefono, Patente, AutoId])
    return result
}