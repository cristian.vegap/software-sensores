import express from "express";
import cors from "cors";
import path from 'path';

//importar database
//import {getAutos} from "./database.js"
import {getAutos, getAuto, createAuto, delAuto, updateAuto} from "./AutoDB.js"
import {getMedicion, getMedicionID, createMedicion, delMedicion, getMedicionesPorDueno } from "./database.js";
//import { getChartURL } from './api.js';

const app = express();
//const path = require('path');
//const filePath = path.join(__dirname,"index.html");
const __dirname = path.resolve();

app.use(express.json())
app.use(cors())
app.use(express.static("public"));


//getChartURL();
app.get("/", (req, res) => {
    //res.send("MAIN");
    res.sendFile(__dirname +"/thingspeak/readData.html");
    //res.sendFile(filePath);
});

/////////////////// AUTOS ////////////////
app.get("/autos/", async (req, res) => {
    const autos = await getAutos()
    res.send(autos)
});

app.get("/autos/:id", async (req, res) => {
    const id = req.params.id
    const auto = await getAuto(id)
    res.send(auto)
})

app.post("/autos", async (req, res) => {
    const { Dueno, Telefono, Patente } = req.body;
    const auto = await createAuto(Dueno, Telefono, Patente);
    res.status(201).send(auto);
});

app.delete("/autos/:id", async (req, res) => {
    const id = req.params.id
    const auto = await delAuto(id)
    res.send(auto)
})

app.put("/autos/:id", async (req, res) => {
    const id = req.params.id
    const { Dueno, Telefono, Patente } = req.body;
    const autoActualizado = await updateAuto(Dueno, Telefono, Patente, id);
    res.send(autoActualizado);
})


/////////////////// MEDICIONES////////////////
// Ruta para obtener todas las mediciones
app.get("/mediciones", async (req, res) => {
    const mediciones = await getMedicion();
    res.send(mediciones);
});

// Ruta para obtener una medición específica por su ID
app.get("/mediciones/:id", async (req, res) => {
    const id = req.params.id;
    const medicion = await getMedicionID(id);
    res.send(medicion);
});

// Ruta para crear una nueva medición
app.post("/mediciones", async (req, res) => {
    const { Auto, Medicion, Fecha } = req.body;

    try {
        const nuevaMedicion = await createMedicion(Auto, Medicion, Fecha);
        res.status(201).send(nuevaMedicion);
    } catch (error) {
        console.error('Error al crear la medición:', error);
        res.status(500).send({ error: 'Error interno del servidor' });
    }
});

// Ruta para eliminar una medición por su ID
app.delete("/mediciones/:id", async (req, res) => {
    const id = req.params.id;

    try {
        const medicionEliminada = await delMedicion(id);
        res.send(medicionEliminada);
    } catch (error) {
        console.error('Error al eliminar la medición:', error);
        res.status(500).send({ error: 'Error interno del servidor' });
    }
});

app.get("/medicionesPorDueno/:dueno", async (req, res) => {
    const dueno = req.params.dueno;

    try {
        const medicionesPorDueno = await getMedicionesPorDueno(dueno);
        res.send(medicionesPorDueno);
    } catch (error) {
        console.error('Error al obtener las mediciones por dueño:', error);
        res.status(500).send({ error: 'Error interno del servidor' });
    }
});

////////////////////////
app.listen(8080, () => {
    console.log("Server listening on port 8080")
});
