import mysql from "mysql2";

//const express = require ("express");
//const app = express();

const pool = mysql.createPool({
    host:"localhost",
    user:"root",
    password:"cristian.vegap",
    database:"ioevdb",
}).promise()

//GET TODO
export async function getMedicion(){
    const [rows] = await pool.query("SELECT * FROM medicion")
    return rows
}

//GET Especifico
export async function getMedicionID(AutoId){
    const [rows] = await pool.query(`
    SELECT *
    FROM medicion
    WHERE Auto = ?
    `, [AutoId])
    return rows
}

//CREAR MEDICION
export async function createMedicion(Auto, Medicion, Fecha) {
    const [result] = await pool.query(`
        INSERT INTO medicion (Auto, Medicion, Fecha)
        VALUES (?, ?, ?)
    `, [Auto, Medicion, Fecha]);

    const id = result.insertId;
    return getMedicionID(id);
}

//DELETE
export async function delMedicion(MedicionId){
    const [rows] = await pool.query(`
    DELETE FROM medicion
    WHERE MedicionId = ?
    `, [MedicionId])
    return rows
}

export async function getMedicionesPorDueno(dueno) {
    const [rows] = await pool.query(`
        SELECT medicion.*
        FROM medicion
        JOIN automovil ON medicion.Auto = automovil.AutoId
        WHERE automovil.Dueno = ?
    `, [dueno]);

    return rows;
}
//getMedicionesPorDueno()